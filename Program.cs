﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Program2
{
    class FindIndex
    {
        static void Main(string[] args)
        {
            int size = Convert.ToInt32(Console.ReadLine());
            int[] arrOfElem = new int[size];
            for (int el = 0; el < size; ++el)
            {
                arrOfElem[el] = Convert.ToInt32(Console.ReadLine());
            }
            int sum1 = arrOfElem[0];
            int sum2 = arrOfElem[size-1];
            int i = 0;
            int j = size-1;
            while (i != j)
            {
                if (sum1 < sum2)
                {
                    sum1 += arrOfElem[i + 1];
                    ++i;
                }
                else
                {
                    sum2 += arrOfElem[j - 1];
                    --j;
                }

            }
            if (sum1 == sum2)
            {
                Console.WriteLine("The index is: " + i);
            }
            else
            {
                Console.WriteLine("The index is not found");
            }
                Console.ReadKey();
        }
    }
}
